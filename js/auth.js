// db1.js
var secret = Buffer.from('fe1a1915a379f3be5394b64d14794932', 'hex');
var jwt = require('jsonwebtoken');
var express = require('express')
const authGuard = express.Router(); 

exports.secret = secret;

module.exports = {
    getSecret: () => {
        return secret;
    },
    verifyToken: () => {
        return authGuard.use((req, res, next) => {
            var token = req.headers["authorization"];
            if(token == null || token == "") {
                return res.sendFile({success: false, msg: "No estás logeado"});
            }
            token = token.replace('Bearer ', '');
            jwt.verify(token, secret, function (err, user) {
                if (err) {
                    return res.json({success: false, msg: "No estás logeado"});
                } else {
                    next();
                }
            });
        });
    },
}